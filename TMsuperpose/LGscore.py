maxpvalue = 1
maxpvalue1 = 1.0

# Input should be two numpy arrays of coordinates 

length = max(coords1.shape[0], coords2.shape[0])

d0_TM = 1.24 * pow(length - 15, 0.3333333333) - 1.8


#bestsizescore = [0] * coords1.shape[0]  # WHat is this ?

i = 0

# Do 

while i < coords1.shape[0]:
    selected[i] = False
    i += 1

i = 0
while i < coords1.shape[0] - L:
    j = 0
    while j < coords1.shape[0]:
        selected[j] = True
        j += 1
    
    j = L

    mol1=f(coord1,selected) # Get the selected coordinates, should be a simple mumpy code
    mol2=f(coord2,selected)
    rms = superimpose(mol1, mol2)    # get rmsd per positions (np.diff)
    # Copy this to all coords (not only selected)
    .rotate 
    count = 0
    
    while j < coords1.shape[0] and rms < factor * (j + 225) / 225: # pass this is variables
        count += 1
        minrmsd = 9999.0
        k = 0
        while k < coords1.shape[0]:
            if not selected[k]:
                if rms < minrmsd:
                    minpos = k
                    minrmsd = rms[k]
                
                if rms[k] < minsim:
                    selected[k] = True
                    j += 1
            k += 1
        
        j += 1
        coords1[minpos].selected = True
        coords2[minpos].selected = True
        
        mol1=f(coord1,selected) # Get the selected coordinates, should be a simple mumpy code
        mol2=f(coord2,selected)
        rms = superimpose_molecules(mol1, mol2) # TMscore
        
        score = Levitt_Gerstein(mol1, mol2, d0 * d0)  # Sscore
        
        if (score >= maxscore) and (j > minatoms):
            maxatoms = j
            maxrms = rms.mean()
            maxscore = score
            
            k = 0
            while k < coords1.shape[0]:
                maxselect[k] = selected[k]
                k += 1
        
        i += step

maxatoms1 = maxatoms
maxrms1 = maxrms
maxscore1 = maxscore
maxpvalue1 = maxpvalue



# Should we not also try starting from all and skip.
