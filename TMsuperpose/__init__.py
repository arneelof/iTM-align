# Copyright (C) 2002, Thomas Hamelryck (thamelry@vub.ac.be)
#
# This file is part of the Biopython distribution and governed by your
# choice of the "Biopython License Agreement" or the "BSD 3-Clause License".
# Please see the LICENSE file that should have been included as part of this
# package.
"""Align on protein structure onto another using SVD alignment.

SVDSuperimposer finds the best rotation and translation to put
two point sets on top of each other (minimizing the RMSD). This is
eg. useful to superimpose crystal structures. SVD stands for singular
value decomposition, which is used in the algorithm.

Also based on the TMscore algorithm by Y. Zhang and J. Skolnick

Also based on BIO.SVDSuperimposer by Thomas Hamelryck

"""


try:
    from numpy import dot, transpose, sqrt
    from numpy.linalg import svd, det
    import numpy as np
    from Bio.SVDSuperimposer import SVDSuperimposer
    from Bio.PDB.PDBExceptions import PDBException

except ImportError:
    from Bio import MissingPythonDependencyError

    raise MissingPythonDependencyError(
        "Install NumPy if you want to use Bio.SVDSuperimposer."
    )
import numpy as np

class TMSuperimposer:
    """Class to run SVD alignment with TMscore optimization.

    """

    def __init__(self):
        """Initialize the class."""
        self._clear()

    # Private methods

    def _clear(self):
        self.rotran = None
        self.TM =None
        self.rms = None
        self.TMrms = None
        self.TMnatoms = None
        # Do we need d0 or something else here as well?

    # We can use the 
    def _rms(self, coords1, coords2,selection=None):
        """Return rms deviations between coords1 and coords2 (PRIVATE)."""
        if selection is None:
            selcoords1 = coords1
            selcoords2 = coords2
        else:
            sel=np.concatenate((selection[:,np.newaxis], selection[:,np.newaxis],selection[:,np.newaxis]), axis=1)
            selcoords1 = coords1[sel].reshape(-1,3)
            selcoords2 = coords2[sel].reshape(-1,3)
        diff = selcoords1 - selcoords2
        return sqrt(sum(sum(diff * diff)) / coords1.shape[0])

    def _rmsres(self, coords1, coords2):
        """Return rms deviations between coords1 and coords2 per residues (PRIVATE)."""
        diff = coords1 - coords2
        sqdiff=np.sum(diff*diff,axis=1)
        return sqrt(sqdiff) # No time is saved by not using sqrt here

    def _TM(self, coords1, coords2,d0=-1):
        """Return rms deviations between coords1 and coords2 (PRIVATE)."""
        """Return TMscore between coords1 and coords2 already translated"""
        if d0 == -1:
            d0 = 1.24 * (max(12,coords1.shape[0] - 15)) ** (1 / 3) - 1.8
            # We do not want d0 to be too small
        d2 = d0 * d0
        #print ("Test1",d0,coords1.shape[0],coords2.shape[0])
        diff = coords1 - coords2
        #print(diff)

        sqdiff=np.sum(diff*diff,axis=1)
        #return sqrt(sum(sum(diff * diff)) / coords1.shape[0])
        #print(1/(1+sqdiff/d2))
        #print("_TM",sum(1/(1+sqdiff/d2)),coords1.shape[0])
        return sum(1/(1+sqdiff/d2))/coords1.shape[0]        

    def _superimpose(self, coords1, coords2,selection=None):
        """Superpose coords2 on coords1 (PRIVATE) using selection ."""
        # Extract coords
        if selection is None:
            selcoords1 = coords1
            selcoords2 = coords2
        else:        
            sel=np.concatenate((selection[:,np.newaxis], selection[:,np.newaxis],selection[:,np.newaxis]), axis=1)
            selcoords1 = coords1[sel].reshape(-1,3)
            selcoords2 = coords2[sel].reshape(-1,3)

        sup=SVDSuperimposer()
        sup.set(selcoords1,selcoords2)
        sup.run()
        return sup.get_rotran()


    # Public methods


    def set_atoms(self, fixed, moving):
        """Prepare translation/rotation to minimize RMSD between atoms.

        Put (translate/rotate) the atoms in fixed on the atoms in
        moving, in such a way that the RMSD is minimized.

        :param fixed: list of (fixed) atoms
        :param moving: list of (moving) atoms
        :type fixed,moving: [L{Atom}, L{Atom},...]
        """
        if not len(fixed) == len(moving):
            raise PDBException("Fixed and moving atom lists differ in size")
        length = len(fixed)
        fixed_coord = np.zeros((length, 3))
        moving_coord = np.zeros((length, 3))
        for i in range(0, length):
            fixed_coord[i] = fixed[i].get_coord()
            moving_coord[i] = moving[i].get_coord()
        sup = SVDSuperimposer()
        sup.set(fixed_coord, moving_coord)
        sup.run()
        self.rms = sup.get_rms()
        (R, T) = sup.get_rotran()
        transformed_coords=dot(moving_coord, R) + T
        self.TM = self._TM(transformed_coords, fixed_coord)
        self.rotran = sup.get_rotran()


    def apply(self, atom_list):
        """Rotate/translate a list of atoms."""
        if self.rotran is None:
            raise PDBException("No transformation has been calculated yet")
        rot, tran = self.rotran
        rot = rot.astype("f")
        tran = tran.astype("f")
        for atom in atom_list:
            atom.transform(rot, tran)



    def setTM(self,fixed,moving,d0=-1,L=4,factor=.5,minsim=7.,minatoms=19,step=2,enough=0.9):
        """Superimpose the coordinate sets using TMscore opimisation from Pcons."""
        if not len(fixed) == len(moving):
            raise PDBException("Fixed and moving atom lists differ in size")
        length = len(fixed)
        coords1 = np.zeros((length, 3)) # fixed
        coords2 = np.zeros((length, 3)) # moving
        for i in range(0, length):
            coords1[i] = fixed[i].get_coord()
            coords2[i] = moving[i].get_coord()
        maxscore = 0.0
        sup=SVDSuperimposer()

        # lets select everything first to try
        selected=np.ones(length,dtype=bool) # All false
        R,T=self._superimpose(coords1, coords2,selected)    # get rmsd per positions (np.diff)
        transformed_coords=dot(coords2, R) + T
        rms = self._rms(transformed_coords, coords1,selected)           
        TM = self._TM(transformed_coords, coords1)
        RMSall=rms
        maxscore = TM
        maxatoms = length
        maxrms = rms
        maxscore = TM
        maxselect = selected.copy()
        rot=R
        trans=T

        # For one example where it matters it takes
        # 60.5s with minsim=5  
        # 52s with minsum=11
        # 9s with minsim=25
        # 4.5s with minsim=5 and enough=0.9
        # Example 2
        # 99s with minsim=5 and enough=0.9
        # 15s with minsim=11 and enough=0.9


        # Now we start the loop
        minsim = minsim * minsim
        i = 0
        while (i < length - L) and (maxscore < enough):
            selected=np.zeros(length,dtype=bool) # All false
            #print (i)
            selected[i:i+L] = True
            j = L


            R,T=self._superimpose(coords1, coords2,selected)    # get rmsd per positions (np.diff)
            transformed_coords=dot(coords2, R) + T
            #print (R,T,coords2,transformed_coords)
            RMSres = self._rmsres(transformed_coords, coords1)
            #print (RMSres)
            rms = self._rms(transformed_coords, coords1,selected)
            #rms2 = sup._rms(transformed_coords, coords1)
            #count = 0
            #print (i,j,length,rms,factor,factor * (j + 225) / 225)
            while ( ( j < length) and (rms <  (factor * (j + 225) / 225))): # pass this is variables
                #count += 1
                minrmsd = 9999.0
                k = 0
                while k < length:
                    if not selected[k]:
                        if RMSres[k] < minrmsd:
                            minpos = k
                            minrmsd = RMSres[k]
                        elif RMSres[k] < minsim:
                            selected[k] = True
                            j += 1
                    k += 1
                
                j += 1
                selected[minpos] = True
                R,T=self._superimpose(coords1, coords2,selected)    # get rmsd per positions (np.diff)
                transformed_coords=dot(coords2, R) + T
                RMSres = self._rmsres(transformed_coords, coords1)
                rms = self._rms(transformed_coords, coords1,selected)
                TM = self._TM(transformed_coords, coords1,d0)
                #print (TM,maxscore,j,minatoms)
                if (TM >= maxscore) and (j > minatoms):
                    maxatoms = j
                    maxrms = rms
                    maxscore = TM
                    maxselect = selected.copy()
                    rot=R
                    trans=T
            i += step
        self.rms = RMSall
        self.rotran = [rot,trans]
        self.TM=maxscore
        self.TMrms=maxrms
        self.TMnatoms=maxatoms

        
